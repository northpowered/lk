from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
import datetime
from .plugreport import Reports

############REPORTS REGISTRY#####
@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.transactionR',login_url='/index'), name='dispatch')
class ReportsRegistryView(View):

    template_name = 'reportsregistry.html'

    def get(self, request, *args, **kwargs):
        context = {}
        mode = request.GET.get("mode")
        date = request.GET.get("date")
        if date == '' or mode == '':
            return render(request, template_name=self.template_name, context=context)
        context['mode'] = mode
        ReportObject = Reports("Registry")

        if mode == 'sum_rep_month':
            report = ReportObject.SumRepMonth(date)
            context['report'] = report
            context['total'] = 0
            for el in report:
                context['total'] += el.get('sum')
        if mode == 'rep_month':
            response = ReportObject.RepMonth(date)
            #print(report)
            context['result']=response

        if mode == 'rep_day':
            report = ReportObject.RepDay(date)
            context['report'] = report
            context['total'] = 0
            for el in report:
                context['total'] += el.get('sum')


        return render(request, template_name=self.template_name, context=context)

############REPORTS PROCESSING#####
@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.transactionR',login_url='/index'), name='dispatch')
class ReportsProcessingView(View):

    template_name = 'reportsprocessing.html'

    def get(self, request, *args, **kwargs):
        context = {}
        mode = request.GET.get("mode")
        date = request.GET.get("date")
        if date == '' or mode == '':
            return render(request, template_name=self.template_name, context=context)
        context['mode'] = mode
        ReportObject = Reports("Processing")

        if mode == 'sum_rep_month':
            report = ReportObject.SumRepMonth(date)
            context['report'] = report
            context['total'] = 0
            for el in report:
                context['total'] += el.get('sum')

        if mode == 'rep_month':
            response = ReportObject.RepMonth(date)
            context['result']=response

        if mode == 'rep_day':
            report = ReportObject.RepDay(date)
            context['report'] = report
            context['total'] = 0
            for el in report:
                context['total'] += el.get('sum')


        return render(request, template_name=self.template_name, context=context)

############REPORTS RAW#####
@method_decorator(login_required, name='dispatch')
class ReportsRawView(View):

    template_name = 'reportsraw.html'

    def get(self, request, *args, **kwargs):
        context = {}
        
        mode = request.GET.get("mode")
        date = request.GET.get("date")
        if date == '' or mode == '':
            return render(request, template_name=self.template_name, context=context)
        context['mode'] = mode
        ReportObject = Reports("Raw")
        

       
        if mode == 'by_gates':
            report = ReportObject.raw_by_gates(date)
            if report:
                context['result'] = report



        if mode == 'by_aft':
            report = ReportObject.raw_aft(date)
            context['result'] = report


        return render(request, template_name=self.template_name, context=context)
