from django.conf.urls import url

from . import views

app_name = 'app_reports'
urlpatterns = [
  url(r'^registry', views.ReportsRegistryView.as_view()),
  url(r'^processing', views.ReportsProcessingView.as_view()),
  url(r'^raw', views.ReportsRawView.as_view()),
]
