from app_registry import models as RegistryModels
from app_processing import models as ProcessingModels
from app_raw import models as RawModels
from django.db.models import Func, F, Sum, Count
import datetime
import calendar
from django.db import connections
from pprint import pprint
class Reports():

    def __init__(self, SOURCE):
        self.SOURCE = SOURCE
        self.MODEL = None
        if self.SOURCE == "Registry":
            self.MODEL = RegistryModels.RegistryModel
        elif self.SOURCE == "Processing":
            self.MODEL = ProcessingModels.ProcessingModel
        elif self.SOURCE == "Raw":
            self.MODEL = RawModels.RawModel
        


    def ParseDate(self, date, get='all'):
        try:
            date = date.split('/')
            f_date = date[2]+'-'+date[0]+'-'+date[1]
        except AttributeError: 
            return None
        except IndexError:
            return None
        if get == 'all':
            return f_date
        elif get == 'day':
            return date[1]
        elif get == 'month':
            return date[0]
        elif get == 'year':
            return date[2]
        else:
            return f_date

    def CreateDate(self, year,month,day):
        return str(year)+'-'+str(month)+'-'+str(day)

    def GetMonthInterval(self, date, side):
        year = int(self.ParseDate(date,get='year'))
        month = int(self.ParseDate(date,get='month'))
        day = calendar.monthrange(year, month)[1]
        if side == 'left':
            return self.CreateDate(year,month,'1')
        if side == 'right':
            return self.CreateDate(year,month,day)


    def SumRepMonth(self, date):
        
        left = self.GetMonthInterval(date,'left')
        right = self.GetMonthInterval(date,'right')
        response = self.MODEL.objects.filter(DocDate__gte=left).filter(DocDate__lte=right).values('DocDate').annotate(sum=Sum('Amount')).order_by()
        return(response)
        
    def RepMonth(self, date):
        left = self.GetMonthInterval(date,'left')
        right = self.GetMonthInterval(date,'right')
        paramsQ = self.MODEL.objects.filter(DocDate__gte=left).filter(DocDate__lte=right).values('Contract')
        params = set()
        for p in paramsQ:
            params.add(p.get('Contract'))

        ret = {}
        dates = set()
        total = []
        for p in params:
            ret[p] = self.MODEL.objects.filter(DocDate__gte=left).filter(DocDate__lte=right).filter(Contract=p).values('DocDate').annotate(sum=Sum('Amount')).order_by()
            promtotal = {'key':p}
            promsum = 0
            for r in ret[p]:
                promsum = promsum + r.get('sum')
                dates.add(r.get('DocDate'))
            promtotal.update({'val':promsum})
            total.append(promtotal)
        l_dates = []
        for d in dates:
            l_dates.append(d)
        l_dates.sort()
        result = []
        for d in l_dates:
            amounts = []
            for p in params:
                #amounts.append({'Contract':p, 'sum':0})
                haveData = False
                for r in ret[p]:
                    
                    if r.get('DocDate') == d:
                        amounts.append({'Contract':p, 'sum':r.get('sum')})
                        haveData = True
                if not haveData:
                    amounts.append({'Contract':p, 'sum':0})

                    
                        
                    
                
            result.append({'DocDate':d,'amount':amounts})
        response={}
        response['total'] = total
        response['params']=params
        response['report']=result
        
        return(response)

    def RepDay(self, date):
        date_f = self.ParseDate(date)
        response = self.MODEL.objects.filter(DocDate=date_f).values('Terminal').annotate(sum=Sum('Amount')).order_by()
        return(response)
            

    def dictfetchall(self, cursor):
        
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def raw_by_gates(self, date):
        left = self.GetMonthInterval(date,'left')
        right = self.GetMonthInterval(date,'right')
    
        with connections['raw_processing'].cursor() as cursor:
            cursor.execute('SELECT gate FROM daily_turnover_by_gates WHERE (operation_date between %(left)s and %(right)s) group by gate order by gate', {'left': left, 'right': right})
            gates = self.dictfetchall(cursor)
        list_gates = []
        set_gates = set()
        print(gates)
        for g in gates:
            set_gates.add(g['gate'])
        for g in set_gates:
            list_gates.append(g)
        #list_gates.sort()
        print(list_gates)
        if len(list_gates) == 0:
            return None
        gates_arg = ", "
        args = []
        for g in list_gates:
            arg = '"'+str(g)+'" text'
            args.append(arg)
        gates_arg = gates_arg.join(args)
        print(gates_arg)
        with connections['raw_processing'].cursor() as cursor:
            query = """select * from crosstab(
    'select DATE(operation_date), gate, SUM(amount) from daily_turnover_by_gates where (operation_date between ''{0}'' AND ''{1}'') group by cube (DATE(operation_date), gate) order by DATE(operation_date)',
    'select gate as gates from daily_turnover_by_gates where (operation_date between ''{0}'' AND ''{1}'') group by gate order by gate') as (operation_date date,{2});""".format(left, right, gates_arg)
            print(query)
            cursor.execute(query)
            report = self.dictfetchall(cursor)
            
            for r in report:
                sum_day = 0
                sums = []
                for gate in list_gates:
                    gate_sum = r.get(str(gate))
                    sums.append(gate_sum) 
                    if gate_sum:
                        
                        gate_sum = gate_sum.replace('.','')
                        
                        sum_day = int(sum_day) + int(gate_sum)
                       
                sum_day_s = str(sum_day)[0:-2]+'.'+str(sum_day)[-2:]
                r['day_sum'] = sum_day_s
                r['sums'] = sums
            
            pprint(report)
            return {'report': report, 'gates': list_gates}

        
    def raw_aft(self, date):
        left = self.GetMonthInterval(date,'left')
        right = self.GetMonthInterval(date,'right')
        with connections['raw_processing'].cursor() as cursor:
            query = "select * from daily_turnover_aft where (operation_date between '{0}' AND '{1}')".format(left, right)
            print(query)
            cursor.execute(query)
            report = self.dictfetchall(cursor)
            pprint(report)
            total = 0
            for r in report:
                total = total + r['amount']
            return {'report': report, 'total': total}
