"""lk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth.views import LogoutView
from lk import settings
from app_lk import views
urlpatterns = [
    path(r'', views.IndexView.as_view()),
    path('index/', views.IndexView.as_view()),
    path('cancel/', views.CancelView.as_view()),
    url(r'^clearing/', include('app_clearing.urls')),
    url(r'^processing/', include('app_processing.urls')),
    url(r'^raw/', include('app_raw.urls')),
    url(r'^registry/', include('app_registry.urls')),
    url(r'^reports/', include('app_reports.urls')),
    url(r'^chargeback/', include('app_chargeback.urls')),
    path('login/', views.LoginView.as_view()),
    path('logout/', LogoutView.as_view()),
    path('admin/', admin.site.urls),
]
