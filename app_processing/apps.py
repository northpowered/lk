from django.apps import AppConfig


class AppProcessingConfig(AppConfig):
    name = 'app_processing'
