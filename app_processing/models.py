from django.db import models
from django.contrib.auth.models import User
import uuid
import datetime
import django.utils


class ProcessingModel(models.Model):
    class Meta:
        db_table = "parsed_operations" 
        permissions = (
            ('transactionR', "Read transactions"),
            ('transactionW', "Write transactions"),
            ('transactionD', "Delete transactions"),
            ('transactionC', "Change transactions"),
        )
    
    UID = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False,db_column='uid') #+
    ID = models.BigIntegerField(unique = True, db_index=True, db_column='id', db_tablespace='pg_default') #+ 
    Contract = models.TextField(default = '', null = False, db_column='contract') #+
    Bank = models.IntegerField(null = False, db_column='bank') #+
    Terminal = models.BigIntegerField(null = False, db_column='terminal') #+
    CardType = models.SmallIntegerField(null = False, db_column='card_type') #+
    CardProduct = models.TextField(null = False, db_column='card_product') #+
    TxnType = models.TextField(unique = True, db_index=True, db_column='txn_type', db_tablespace='pg_default') #+ 
    Amount = models.BigIntegerField(unique = True, db_index=True, db_column='amount', db_tablespace='pg_default') #+ 
    Currency = models.TextField(null = False, db_column='currency') #+
    Fee = models.BigIntegerField(default = 0, null=False, db_column='fee') #+
    TxnDateTime = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='txn_date_time', db_tablespace='pg_default') #+ 
    OrigDate = models.DateField(null = False, default = django.utils.timezone.now, db_column='orig_date') #+
    DocDate = models.DateField(null = False, default = django.utils.timezone.now, db_column='doc_date') #+
    Sequence = models.TextField(null = False, db_column='sequence') #+
    Reparation = models.BigIntegerField(null = False, db_column='reparation') #+
    Card = models.TextField(null = False, db_column='card') #+
    AuthCode = models.TextField(null = False, db_column='auth_code') #+
    RRN = models.TextField(null = False, db_index=True, db_column='rrn', db_tablespace='pg_default') #+ 
    OrderID = models.TextField(null = False, db_index=True, db_column='order_id', db_tablespace='pg_default') #+ 
    TcFee = models.BigIntegerField(null = False, db_column='tc_fee') #+
    VerifyStatus = models.IntegerField(null = False, default = 0, db_column='verify_status') #+

    def __str__(self):
        return str(self.UID)
