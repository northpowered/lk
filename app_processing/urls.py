from django.conf.urls import url

from . import views

app_name = 'app_processing'
urlpatterns = [
  url(r'^$', views.ProcessingTableView.as_view()),
  url(r'^search', views.ProcessingSearchView.as_view()),

]
