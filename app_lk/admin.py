from django.contrib import admin
from .models import ChargebackModel, CancelModel
# Register your models here.

admin.site.register(ChargebackModel)
admin.site.register(CancelModel)

