from django.db import models
from django.contrib.auth.models import User
import uuid
import datetime
import django.utils
# Create your models here.

class ChargebackModel(models.Model):
    class Meta:
        db_table = "chargeback"
        permissions = (
            ('chargebackR', "Read chargebacks"),
            ('chargebackW', "Write chargebacks"),
            ('chargebackD', "Delete chargebacks"),
            ('chargebackC', "Change chargebacks"),
        )
        
    ID = models.BigIntegerField(primary_key=True, editable=False)

    def __str__(self):
        return self.ID

class CancelModel(models.Model):
    class Meta:
        db_table = "cancel"
        permissions = (
            ('cancelR', "Read cancels"),
            ('cancelW', "Write cancels"),
            ('cancelD', "Delete cancels"),
            ('cancelC', "Change cancels"),
        )
        
    ID = models.BigIntegerField(primary_key=True, editable=False)

    def __str__(self):
        return self.ID   


