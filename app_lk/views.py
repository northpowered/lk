from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page
from .models import *
from .graphgenerator import *
import datetime
# Create your views here.


##############INDEX PAGE################
@method_decorator(login_required, name='dispatch')
#@method_decorator(cache_page(60 * 60), name='dispatch')
#@cache_page(60 * 1)
class IndexView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        context = {}
        #GraphsR = GraphsFromRegistry()
        #context['ByContracts'] = GraphsR.ByContractsGraphs(9)
        #context['SumGraph'] = GraphsR.SumGraph(9)
        return render(request, template_name=self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.chargebackR',login_url='/index'), name='dispatch')
class ChargebackView(View):
    template_name = 'chargeback.html'

    def get(self, request, *args, **kwargs):

 
        return render(request, template_name=self.template_name)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.cancelR',login_url='/index'), name='dispatch')
class CancelView(View):
    template_name = 'cancel.html'

    def get(self, request, *args, **kwargs):

 
        return render(request, template_name=self.template_name)


class LoginView(View):
    template_name = 'login.html'
    
    def post(self, request, *args, **kwargs):
        print('1')
        username = request.POST.get('login','')
        password = request.POST.get('password', '') 
        user = authenticate(username=username, password=password)
        if user is not None:
            print('2')
            login(request, user)
            return redirect('/index/')
        else:
            print('3')
            login_error = 'User not exist'
            context = {'login_error': login_error}
            return render(request, template_name=self.template_name)



    def get(self, request, *args, **kwargs):
        print('4')
        return render(request, template_name=self.template_name)



class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/login/")
