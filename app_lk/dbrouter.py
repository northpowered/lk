from app_lk.models import CancelModel, ChargebackModel
from app_processing.models import ProcessingModel
from app_clearing.models import EventsModel
from app_registry.models import RegistryModel
from app_chargeback.models import ChargebackLoggingModel, ChargebackRegistryModel
from app_raw.models import RawModel
class MyDBRouter(object):

    def db_for_read(self, model, **hints):
      
        if model == ProcessingModel:
            return 'processing'
        if model == ChargebackModel:
            return 'processing'
        if model == CancelModel:
            return 'processing'
        if model == RegistryModel:
            return 'registry'
        if model == EventsModel:
            return 'clearing'
        if model == ChargebackLoggingModel:
            return 'default'
        if model == ChargebackRegistryModel:
            return 'default'
        if model == RawModel:
            return 'raw_processing'
        return None

