from app_registry import models
from django.db.models import Func, F, Sum, Count
import datetime
import calendar

class GraphsFromRegistry():

    def __init__(self):
        pass


    def ParseDate(self, date, get='all'):
        try:
            date = date.split('/')
            f_date = date[2]+'-'+date[0]+'-'+date[1]
        except AttributeError: 
            return None
        except IndexError:
            return None
        if get == 'all':
            return f_date
        elif get == 'day':
            return date[1]
        elif get == 'month':
            return date[0]
        elif get == 'year':
            return date[2]
        else:
            return f_date

    def CreateDate(self, year,month,day):
        return year+'-'+month+'-'+day


    def ByContractsGraphs(self, interval):
        response = {}
        finish = datetime.datetime.today()
        start = finish - datetime.timedelta(days=interval)
        f_finish = self.CreateDate(str(finish.year), str(finish.month), str(finish.day))
        f_start = self.CreateDate(str(start.year), str(start.month), str(start.day))
        paramsQ = models.RegistryModel.objects.filter(DocDate__gte=f_start).filter(DocDate__lte=f_finish).values('Contract')
        paramsS = set()
        params = []
        for p in paramsQ:
            paramsS.add(p.get('Contract'))
        for p in paramsS:
            params.append(p)
        params.sort()

        response['contracts'] = params

        ret = {}
        dates = set()
 
        for p in params:
            ret[p] = models.RegistryModel.objects.filter(DocDate__gte=f_start).filter(DocDate__lte=f_finish).filter(Contract=p).values('DocDate').annotate(sum=Sum('Amount')).order_by()

            for r in ret[p]:
                dates.add(r.get('DocDate')) 
        #print(ret)
        l_dates = []
        for d in dates:
            l_dates.append(d)
        l_dates.sort()

        reformed_ret = {}
        for p in params:
            reformed_ret[p] = {}
            for r in ret[p]:
                reformed_ret[p].update({r['DocDate']:r['sum']})

        #print(reformed_ret)
        series = []
        for p in params:
            one_series = {}
            one_series['key'] = p
            val = []
            for ld in l_dates:
                check_date = reformed_ret[p].get(ld)
                if check_date != None:
                    val.append(check_date/100000000)
                else:
                    val.append(0)
            one_series['val'] = val
            series.append(one_series)
        
        #print(series)
        #print(l_dates)
        f_dates = []
        for ld in l_dates:
            f_dates.append(str(self.CreateDate(str(ld.year), str(ld.month), str(ld.day))))
        #print(f_dates)
        #print(series)
        response['dates'] = f_dates
        response['series'] = series
        return response

    def SumGraph(self, interval):
        response = {}
        finish = datetime.datetime.today()
        start = finish - datetime.timedelta(days=interval)
        f_finish = self.CreateDate(str(finish.year), str(finish.month), str(finish.day))
        f_start = self.CreateDate(str(start.year), str(start.month), str(start.day))
        ret = models.RegistryModel.objects.filter(DocDate__gte=f_start).filter(DocDate__lte=f_finish).values('DocDate').annotate(sum=Sum('Amount')).order_by()
        dates = []
        val = []
        for r in ret:
            dates.append(str(self.CreateDate(str(r['DocDate'].year), str(r['DocDate'].month), str(r['DocDate'].day))))
            val.append(r['sum']/100000000)
        response['dates'] = dates
        response['val'] = val
        return response