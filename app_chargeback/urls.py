from django.conf.urls import url

from . import views

app_name = 'app_chargeback'
urlpatterns = [
  url(r'^$', views.ChargebackIndexView.as_view()),
  url(r'^check', views.ChargebackRRNCheckView.as_view()),
  url(r'^log', views.ChargebackRRNLogView.as_view()),
  url(r'^table', views.ChargebackRRNTable.as_view()),
]
