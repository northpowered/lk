from django.apps import AppConfig


class AppChargebackConfig(AppConfig):
    name = 'app_chargeback'
