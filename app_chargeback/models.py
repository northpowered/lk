from django.db import models
import django.utils
import uuid
import datetime

class ChargebackRegistryModel(models.Model):

    class Meta:
        ordering = ['-TxnDateTime']
        
    UID = models.UUIDField(default = uuid.uuid4, editable = False,db_column='id')  
    ID = models.TextField(db_index=True, db_column='transaction_id', db_tablespace='pg_default')   
    Contract = models.TextField(default = '', null = True, db_column='contract_num')  
    Organisation = models.TextField(null = True, db_column='organisation')  
    Terminal = models.TextField(null = True, db_column='terminal_number')  
    TxnType = models.TextField(db_index=True, db_column='type_of_transaction', db_tablespace='pg_default')  
    TxnChannel = models.TextField(default = '', null = True, db_column='transaction_channel')  
    TypeOfRequest = models.TextField(default = '', null = True, db_column='type_of_request')  
    Amount = models.BigIntegerField(db_index=True, db_column='amount', db_tablespace='pg_default') 
    Commission = models.BigIntegerField(db_index=True, db_column='commission', db_tablespace='pg_default') 
    TxnDateTime = models.DateTimeField(default = django.utils.timezone.now, db_index=True, db_column='transaction_time', db_tablespace='pg_default')   
    DocDate = models.DateField(null = True, default = django.utils.timezone.now, db_column='document_date')   
    OrigDate = models.DateField(null = True, default = django.utils.timezone.now, db_column='original_document_date')
    Reparation = models.BigIntegerField(null = True, db_column='reparation_amount')  
    Card = models.TextField(null = True, db_column='card_number')  
    AuthCode = models.TextField(null = True, db_column='authorization_code') 
    RRN = models.TextField(primary_key = True, null = False, db_index=True, db_column='rrn', db_tablespace='pg_default')   
    OrderID = models.TextField(null = True, db_index=True, db_column='order_id', db_tablespace='pg_default') 
    TecCommission = models.BigIntegerField(default = 0, null=False, db_column='technology_commission') 
    ReportID = models.TextField(null = True, db_index=True, db_column='report_id')  
    VerifyStatus = models.IntegerField(null = True, default = 0, db_column='verify_status')
    ARN = models.TextField(null = True, db_index=True, db_column='arn') 
    Sequence = models.TextField(null = True, db_column='sequence_number') 
    Gate = models.BigIntegerField(null=True, db_column='gate')  

    def __str__(self):
        return str(self.RRN)




class ChargebackLoggingModel(models.Model):

    #ID = models.AutoField(null=False, unique=True, editable=False)
    ActionTimestamp = models.DateTimeField(null = False)
    EditedRRN = models.TextField(null = False)
    EditedAmount = models.BigIntegerField(null = False)
    NewAmount = models.BigIntegerField(null = False)
    UserName = models.TextField(null = False)
    UserIP = models.TextField(null = False)
    UserAgent = models.TextField(null = False)
    Transaction = models.OneToOneField(ChargebackRegistryModel, on_delete=models.CASCADE, primary_key=True, default='')

    def __str__(self):
        return str(self.EditedRRN)
