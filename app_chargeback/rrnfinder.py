# -*- coding: utf-8 -*-

from app_registry import models
from django.core.exceptions import MultipleObjectsReturned
from django.db import IntegrityError
from django.db.models import Func, F, Sum, Count
import datetime
import calendar
from .models import ChargebackLoggingModel, ChargebackRegistryModel
import xlsxwriter
from app_processing import models as PModels

class RRNEditor():

    def __init__(self,rrn,amount,loginfo):
        self.rrn = rrn
        self.amount = amount
        self.loginfo = loginfo
        self.oldamount = ''
        self.reg_entry = None
        self.error = None

    def Cleaner(self):
        self.rrn = self.rrn.strip()
        self.amount = self.amount.strip()
        self.amount = self.amount.replace('.','')
        self.amount = self.amount.replace(',','')
        self.amount = self.amount.replace(' ','')

    def Editor(self):
        try:
            txnobj = models.RegistryModel.objects.filter(RRN = self.rrn).get(TypeOfRequest = 'Advice')
        except models.RegistryModel.DoesNotExist:
            self.error = "Не найден RRN %s " % self.rrn
            return
        print(self.rrn)
        print(txnobj)
        #txnobj = txnobj[0]
        self.oldamount = txnobj.Amount
        self.reg_entry = ChargebackRegistryModel(UID = txnobj.UID,
        ID = txnobj.ID,
        Contract = txnobj.Contract,
        Organisation = txnobj.Organisation,
        Terminal = txnobj.Terminal,
        TxnType = txnobj.TxnType,
        TxnChannel = txnobj.TxnChannel,
        TypeOfRequest = txnobj.TypeOfRequest,
        Amount = -int(self.amount),
        Commission = 0,
        TxnDateTime = txnobj.TxnDateTime,
        DocDate = txnobj.DocDate,
        OrigDate = txnobj.OrigDate,
        Reparation = -int(self.amount),
        Card = txnobj.Card,
        AuthCode = txnobj.AuthCode,
        RRN = self.rrn,
        OrderID = txnobj.OrderID,
        TecCommission = 0,
        ReportID = txnobj.ReportID,
        VerifyStatus = txnobj.VerifyStatus,
        ARN = txnobj.ARN,
        Sequence = self.rrn,
        Gate = txnobj.Gate
        
        )
       # try:
        self.reg_entry.save()
       # except IntegrityError:
       #     self.error = "Нельзя провести Chargeback по %s дважды!" % self.rrn

        

    def Log(self):
        if self.error != None:
            return
        try:
            logobj = ChargebackLoggingModel.objects.create(ActionTimestamp = datetime.datetime.now(),EditedRRN = self.rrn,EditedAmount = self.oldamount,NewAmount = self.amount,UserName = self.loginfo['username'],UserIP = self.loginfo['ip'],UserAgent = self.loginfo['agent'],Transaction = self.reg_entry)
            logobj.save()
        except IntegrityError:
            self.error = "Нельзя провести Chargeback по %s дважды!" % self.rrn


    def Work(self):
        self.Cleaner()
        self.Editor()
        self.Log()
        


class RRNFinder():

    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.list_data = []
        self.rrn_list = []
        self.amount_list = []
        self.dicts_in_list_data = []
        self.checked_response = []
        self.rows = []

    def Splitter(self):
        if self.raw_data == '':
            return None
        self.list_data = self.raw_data.split('\n')
    
    def Cleaner(self):
        for el in self.list_data:
            self.list_data.remove(el)
            el = el.replace('\r','')
            el = el.replace('\f','')
            el = el.replace('\t','')
            el = el.replace('\v','')
            self.list_data.append(el)
            self.list_data.reverse()

    def Devider(self):
        for el in self.list_data:
            unformatted_list = el.split(' ')
            rrn = unformatted_list.pop(0)
            raw_amount = ''
            for el in unformatted_list:
                raw_amount = raw_amount + str(el)
            raw_amount = raw_amount.replace(' ','')
            raw_amount = raw_amount.replace(',','')
            raw_amount = raw_amount.replace('.','')
            raw_amount = raw_amount.replace('\r','')
            raw_amount = raw_amount.replace('\f','')
            raw_amount = raw_amount.replace('\t','')
            raw_amount = raw_amount.replace('\v','')
            if rrn != '' and raw_amount != '':
                self.rrn_list.append(rrn)
                self.amount_list.append(raw_amount)
                pos_dict = {'rrn':rrn, 'amount':raw_amount}
                self.dicts_in_list_data.append(pos_dict)

    def Finder(self):
        response = models.RegistryModel.objects.filter(TypeOfRequest='Advice').filter(RRN__in=self.rrn_list)
        self.rows = response
        for el in response:


            for pos in self.dicts_in_list_data:
                if pos['rrn'] == el.RRN:
                    check_flag = 0 
#KOSTYLI START
                    
                    logamount = pos['amount']
                    logdata = None
                    logresponse = ChargebackLoggingModel.objects.filter(EditedRRN=pos['rrn'])
                   # print('==================')
                   # print(logresponse)
                  #  print('==================')
                    try:
                        logdata = logresponse[0]
                    except IndexError:
                        logamount = pos['amount']
                    
                    
                    if logdata != None:
                        logamount = logdata.NewAmount
                        check_flag = 2
                    #    print('Exists in log!')
#KOSTYLI END
                     
                    elif str(el.Amount) == str(logamount):   #pos['amount'] -> logamount
                        check_flag = 1
                    checked_dict = {'set':el,'flag':check_flag, 'entered_amount':logamount} #pos['amount'] -> logamount
                    self.checked_response.append(checked_dict)

    

    def Work(self):
        self.Splitter()
        self.Cleaner()
        self.Devider()
        self.Finder()

    
        
class RRNExcel():

    def __init__(self,raw_data, loginfo):
        self.raw = raw_data
        self.rows = []
        self.d_in_list = []
        self.header = ['ID', 'Контракт', 'Организация',	'Номер терминала', 'Тип', 'Канал', 'Категория запроса', 'Сумма', 'Сумма комиссии',
            'Дата и время транзакции',
            'Дата документа',
            'Дата оригинального документа',
            'Сумма возмещения',
            'Номер карты',
            'Код авторизации',
            'RRN',
            'OrderID',
            'Сумма комиссии технолога',
            'ARN',
            'Sequence Number',
            'Платежный шлюз'
        ]
        self.filename=''
        self.commonname = ''
        self.processingdict = {}
        self.loginfo = loginfo
        self.errors = []

    def Finder(self):
        RRNObject = RRNFinder(self.raw)
        RRNObject.Work()
        self.rows = RRNObject.rows
        self.d_in_list = RRNObject.dicts_in_list_data

        tnxresponse = PModels.ProcessingModel.objects.filter(RRN__in=RRNObject.rrn_list)
        
        for el in tnxresponse:
            self.processingdict[el.RRN] = el.Card
        
    def Charger(self):
        print(self.d_in_list)
        for el in self.d_in_list:
            EditObject = RRNEditor(el['rrn'], el['amount'], self.loginfo)
            EditObject.Work()
            if EditObject.error != None:
                self.errors.append(EditObject.error)


    def Generator(self):
        self.commonname = "Forvard_chargebacks_%s.xlsx"%datetime.date.today().strftime("%d.%m.%Y")
        self.filename = "files/chargeback/"+self.commonname
        workbook = xlsxwriter.Workbook(self.filename)
        bold = workbook.add_format({'bold': True})
        worksheet = workbook.add_worksheet()
        worksheet.name = datetime.date.today().strftime("%d.%m.%Y")
        worksheet.write(0,0,"Выписка по Chargebacks (АО \"КИВИ БАНК\") от %s"%datetime.date.today().strftime("%d.%m.%Y"), bold)
        #print(self.rows)
        for i in range(len(self.header)):
            worksheet.write(1,i,self.header[i], bold)

        row_num = 2
        for row in self.rows:

            logamount = ''
            logdata = None
            logresponse = ChargebackLoggingModel.objects.filter(EditedRRN=row.RRN).order_by('-ActionTimestamp')
            try:
                logdata = logresponse[0]
            except IndexError:
                logamount = row.Amount
            
            
            if logdata != None:
                logamount = logdata.NewAmount
            
            print(logdata)    






            worksheet.write(row_num,1,row.Contract) 
            worksheet.write(row_num,2,row.Organisation) 
            worksheet.write(row_num,3,row.Terminal) 
            worksheet.write(row_num,4,"Chargeback")
            worksheet.write(row_num,5,row.TxnChannel) 
            worksheet.write(row_num,6,"Chargeback")
            worksheet.write(row_num,7,-int(logamount)/100.0) #!!!
            worksheet.write(row_num,8,0.0)
            worksheet.write(row_num,9,row.TxnDateTime.strftime("%d.%m.%Y %H:%M:%S"))
            worksheet.write(row_num,10,datetime.date.today().strftime("%d.%m.%Y")) 
            worksheet.write(row_num,11,row.OrigDate.strftime("%d.%m.%Y"))
            worksheet.write(row_num,12,-int(logamount)/100.0) #!!!
            t = ''
            try:
                t = self.processingdict[row.RRN]
            except KeyError:
                t = ''
            worksheet.write(row_num,13,t) 
            worksheet.write(row_num,14,row.AuthCode) 
            worksheet.write(row_num,16,row.OrderID)
            worksheet.write(row_num,17,0.0)
            worksheet.write(row_num,19,row.RRN)
            worksheet.write(row_num,20,row.Gate)

            row_num += 1

        worksheet.autofilter("A2:U%d"%row_num)

        workbook.close()

    def Work(self):
        self.Finder()
        self.Charger()
        self.Generator()
