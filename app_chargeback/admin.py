from django.contrib import admin
from .models import ChargebackLoggingModel, ChargebackRegistryModel
# Register your models here.

admin.site.register(ChargebackLoggingModel)
admin.site.register(ChargebackRegistryModel)

