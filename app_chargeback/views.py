# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
import datetime
from .rrnfinder import RRNFinder, RRNEditor, RRNExcel
from django.http import FileResponse

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.chargebackR',login_url='/index'), name='dispatch')
class ChargebackIndexView(View):
    template_name = 'chargebackindex.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.chargebackR',login_url='/index'), name='dispatch')
class ChargebackRRNCheckView(View):
    template_name = 'chargebackrrncheck.html'

    def get(self, request, *args, **kwargs):
        context = {}
        
        rrn_area = request.GET.get("rrn_area")
        download = request.GET.get("download")

        if download != None and rrn_area != None:

            loginfo = {'username':request.user.username, 'ip':request.META['HTTP_X_REAL_IP'], 'agent':request.META['HTTP_USER_AGENT']}
            Generator = RRNExcel(rrn_area, loginfo)
            Generator.Work()
            print(Generator.errors)
            response = FileResponse(open(Generator.filename, 'rb'),filename='test.xlsx')
            response['Content-Disposition'] = 'attachment; filename=' + Generator.commonname
            return response

        #if edit_amount != None and edit_rrn != None:
        #    print(edit_rrn)
       #     print(edit_amount)
        #    loginfo = {'username':request.user.username, 'ip':request.META['HTTP_X_REAL_IP'], 'agent':request.META['HTTP_USER_AGENT']}
        #    EDITORObject = RRNEditor(edit_rrn,edit_amount,loginfo)
        #    EDITORObject.Work()
        #    context['errors'] = EDITORObject.errors


        if rrn_area != None:

            RRNObject = RRNFinder(rrn_area)
            RRNObject.Work()
            context['have_results'] = True
            context['results'] = RRNObject.checked_response
            context['raw_data'] = rrn_area
        return render(request, template_name=self.template_name, context=context)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.chargebackR',login_url='/index'), name='dispatch')

class ChargebackRRNLogView(View):
    template_name = 'chargebackrrnlog.html'

    def get(self, request, *args, **kwargs):
        context = {}
        response = ChargebackLoggingModel.objects.all()
        context['logs'] = response
        
        return render(request, template_name=self.template_name, context=context)

class ChargebackRRNTable(View):
    template_name = 'chargebackrrntable.html'

    def get(self, request, *args, **kwargs):
        context = {}
        trans = ChargebackRegistryModel.objects.all()
        current_page = Paginator(trans,15)
        page = request.GET.get('page')
        try:
            context['trans'] = current_page.page(page)  
        except PageNotAnInteger:
            context['trans'] = current_page.page(1)  
        except EmptyPage:
            context['trans'] = current_page.page(current_page.num_pages) 
            
        return render(request, template_name=self.template_name, context=context)