# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Operationlog(models.Model):
    uuid = models.UUIDField(primary_key=True)
    id = models.BigIntegerField(blank=True, null=True)
    operationdt = models.DateTimeField(blank=True, null=True)
    client = models.BigIntegerField(blank=True, null=True)
    subclient = models.BigIntegerField()
    gate = models.BigIntegerField(blank=True, null=True)
    protocol = models.BigIntegerField(blank=True, null=True)
    operation = models.BigIntegerField(blank=True, null=True)
    state = models.BigIntegerField(blank=True, null=True)
    gateresultcode = models.IntegerField()
    gateresultmessage = models.CharField(max_length=1000)
    bin = models.BigIntegerField(blank=True, null=True)
    bindst = models.BigIntegerField(blank=True, null=True)
    route = models.BigIntegerField()
    cardtoken = models.BigIntegerField(blank=True, null=True)
    orderid = models.CharField(max_length=128, blank=True, null=True)
    transactionid = models.CharField(max_length=128, blank=True, null=True)
    intref = models.CharField(max_length=50)
    authcode = models.CharField(max_length=50)
    retrefnumber = models.CharField(max_length=50)
    terminal = models.CharField(max_length=128)
    terminalrouted = models.CharField(max_length=128)
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    fee = models.DecimalField(max_digits=11, decimal_places=2)
    logrefsjson = models.TextField()
    processingtime = models.FloatField()
    gateresultcode2 = models.IntegerField()
    gateresultmessage2 = models.CharField(max_length=1000)
    pan = models.CharField(max_length=128)
    country = models.CharField(max_length=3)
    memberid = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    ch = models.CharField(max_length=50)
    productcode = models.CharField(max_length=10)
    productname = models.CharField(max_length=100)
    usg = models.CharField(max_length=50)
    pandst = models.CharField(max_length=128)
    countrydst = models.CharField(max_length=3)
    memberiddst = models.CharField(max_length=20)
    namedst = models.CharField(max_length=100)
    chdst = models.CharField(max_length=50)
    productcodedst = models.CharField(max_length=10)
    productnamedst = models.CharField(max_length=100)
    usgdst = models.CharField(max_length=50)
    orderid2 = models.CharField(max_length=128)
    sequencenumber = models.CharField(max_length=128)
    resendcount = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'operationlog'
