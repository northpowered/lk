from django.db import models
import django.utils

class RawModel(models.Model):

    uuid = models.UUIDField(primary_key=True)#
    id = models.BigIntegerField(blank=True, null=True)#
    operationdt = models.DateTimeField(blank=True, null=True)#
    client = models.BigIntegerField(blank=True, null=True)#
    subclient = models.BigIntegerField()#
    gate = models.BigIntegerField(blank=True, null=True)#
    protocol = models.BigIntegerField(blank=True, null=True)#
    operation = models.BigIntegerField(blank=True, null=True)#
    state = models.BigIntegerField(blank=True, null=True)#
    gateresultcode = models.IntegerField()#
    gateresultmessage = models.CharField(max_length=1000)#
    bin = models.BigIntegerField(blank=True, null=True)#
    bindst = models.BigIntegerField(blank=True, null=True)
    route = models.BigIntegerField()
    cardtoken = models.BigIntegerField(blank=True, null=True)
    orderid = models.CharField(max_length=128, blank=True, null=True)
    transactionid = models.CharField(max_length=128, blank=True, null=True)
    intref = models.CharField(max_length=50)
    authcode = models.CharField(max_length=50)
    retrefnumber = models.CharField(max_length=50)
    terminal = models.CharField(max_length=128)
    terminalrouted = models.CharField(max_length=128)#
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    fee = models.DecimalField(max_digits=11, decimal_places=2)
    logrefsjson = models.TextField()
    processingtime = models.FloatField()
    gateresultcode2 = models.IntegerField()
    gateresultmessage2 = models.CharField(max_length=1000)
    pan = models.CharField(max_length=128)
    country = models.CharField(max_length=3)
    memberid = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    ch = models.CharField(max_length=50)
    productcode = models.CharField(max_length=10)
    productname = models.CharField(max_length=100)
    usg = models.CharField(max_length=50)
    pandst = models.CharField(max_length=128)
    countrydst = models.CharField(max_length=3)
    memberiddst = models.CharField(max_length=20)
    namedst = models.CharField(max_length=100)
    chdst = models.CharField(max_length=50)
    productcodedst = models.CharField(max_length=10)#
    productnamedst = models.CharField(max_length=100)
    usgdst = models.CharField(max_length=50)
    orderid2 = models.CharField(max_length=128)
    sequencenumber = models.CharField(max_length=128)
    resendcount = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'operationlog'
        
class DailyTurnoverByGatesModel(models.Model):
    gate = models.BigIntegerField(blank=True, null=True)
    operation_date = models.DateField(blank=True, null=True)
    amount = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'daily_turnover_by_gates'


class DailyTurnoverAftModel(models.Model):
    operation_date = models.DateField(blank=True, null=True)
    amount = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'daily_turnover_aft'



