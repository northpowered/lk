from django.apps import AppConfig


class AppRawConfig(AppConfig):
    name = 'app_raw'
