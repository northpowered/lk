from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
import datetime
from django.utils.functional import cached_property
from rest_framework.pagination import PageNumberPagination
from django.db.models import Count
"""
class FasterDjangoPaginator(Paginator):
    @cached_property
    def count(self):
        # only select 'id' for counting, much cheaper
        return self.object_list.values('uuid').count()


class FasterPageNumberPagination(PageNumberPagination):
    django_paginator_class = FasterDjangoPaginator

"""
############RAW FULL TABLE#####
@method_decorator(login_required, name='dispatch')
class RawTableView(View):

    template_name = 'rawtable.html'

    def get(self, request, *args, **kwargs):
        context = {}
        #print(1)
        trans = RawModel.objects.all().order_by('-operationdt')
        #try_to_count = RawModel.objects.raw('SELECT count("uuid") FROM operationlog')
        #print(try_to_count[0])
        #try_to_count = RawModel.objects.annotate(Count('uuid'))
        #print(len(try_to_count))
        #print(trans[0])
        #print(2)
        current_page = Paginator(trans,15)
        #print(3)
        page = request.GET.get('page')
        try:
            context['trans'] = current_page.page(page) 
        except PageNotAnInteger:
            context['trans'] = current_page.page(1)  
        except EmptyPage:
            context['trans'] = current_page.page(current_page.num_pages) 
        #print(context['trans']) 
        #print(4)
        return render(request, template_name=self.template_name, context=context)
