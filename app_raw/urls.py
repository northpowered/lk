from django.conf.urls import url

from . import views

app_name = 'app_raw'
urlpatterns = [
  url(r'^$', views.RawTableView.as_view()),
#  url(r'^search', views.ProcessingSearchView.as_view()),

]
