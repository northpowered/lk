from django.db import models
from django.contrib.auth.models import User
import uuid
import datetime
import django.utils

class RegistryModel(models.Model):
    class Meta:
        db_table = "transactions" 
        ordering = ['-TxnDateTime']
        permissions = (
            ('regforvardR', "Read FORVARDtransactions"),
            ('regforvardW', "Write FORVARD transactions"),
            ('regforvardD', "Delete FORVARD transactions"),
            ('regforvardC', "Change FORVARD transactions"),
        )
    UID = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False,db_column='id')  #+
    ID = models.TextField(unique = True, db_index=True, db_column='transaction_id', db_tablespace='pg_default')   #+
    Contract = models.TextField(default = '', null = False, db_column='contract_num')  #+
    Organisation = models.TextField(null = False, db_column='organisation')  #+
    Terminal = models.TextField(null = False, db_column='terminal_number')  #+
    TxnType = models.TextField(unique = True, db_index=True, db_column='type_of_transaction', db_tablespace='pg_default')  #+
    TxnChannel = models.TextField(default = '', null = False, db_column='transaction_channel')  #+
    TypeOfRequest = models.TextField(default = '', null = False, db_column='type_of_request')  #+
    Amount = models.BigIntegerField(unique = True, db_index=True, db_column='amount', db_tablespace='pg_default') #+
    Commission = models.BigIntegerField(unique = True, db_index=True, db_column='commission', db_tablespace='pg_default') #+
    TxnDateTime = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='transaction_time', db_tablespace='pg_default') #+  
    DocDate = models.DateField(null = False, default = django.utils.timezone.now, db_column='document_date')#+   
    OrigDate = models.DateField(null = False, default = django.utils.timezone.now, db_column='original_document_date')#+
    Reparation = models.BigIntegerField(null = False, db_column='reparation_amount')  
    Card = models.TextField(null = False, db_column='card_number')  #+
    AuthCode = models.TextField(null = False, db_column='authorization_code') #+
    RRN = models.TextField(null = False, db_index=True, db_column='rrn', db_tablespace='rrn')   #+
    OrderID = models.TextField(null = False, db_index=True, db_column='order_id', db_tablespace='order_id') #+
    TecCommission = models.BigIntegerField(default = 0, null=False, db_column='technology_commission') #+
    ReportID = models.TextField(null = False, db_index=True, db_column='report_id')  #+
    VerifyStatus = models.IntegerField(null = False, default = 0, db_column='verify_status')#+
    ARN = models.TextField(null = False, db_index=True, db_column='arn') #+
    Sequence = models.TextField(null = False, db_column='sequence_number') #+
    Gate = models.BigIntegerField(null=True, db_column='gate')  #+

    def __str__(self):
        return str(self.UID)