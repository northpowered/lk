from django.apps import AppConfig


class AppRegistryConfig(AppConfig):
    name = 'app_registry'
