from django.conf.urls import url

from . import views

app_name = 'app_registry'
urlpatterns = [
  url(r'^$', views.RegistryTableView.as_view()),
  url(r'^search', views.RegistrySearchView.as_view()),

]
