from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
import datetime


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.transactionR',login_url='/index'), name='dispatch')
class RegistryTableView(View):

    template_name = 'registrytable.html'

    def get(self, request, *args, **kwargs):
        context = {}
        trans = RegistryModel.objects.all() #.order_by('-TxnDateTime')       
        current_page = Paginator(trans,15)
        page = request.GET.get('page')
        try:
            context['trans'] = current_page.page(page)  
        except PageNotAnInteger:
            context['trans'] = current_page.page(1)  
        except EmptyPage:
            context['trans'] = current_page.page(current_page.num_pages) 
        return render(request, template_name=self.template_name, context=context)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('app_lk.transactionR',login_url='/index'), name='dispatch')
class RegistrySearchView(View):
    template_name = 'registrysearch.html'

    def get(self, request, *args, **kwargs):
        context = {}
        mode = request.GET.get("mode")
        page = request.GET.get('page')
        if mode == 'search' or page != None:
            print('1')
            search1 = None
            try:
                search1 = request.GET.get("search1")            
            except AttributeError:
                search1 = None
            print(search1)
            f_date1 = None
            f_date2 = None
            if search1 != None:
                  date = request.GET.get("search_daterange").split(' - ')
                  time1 = request.GET.get("search_time1")+':00.000000'
                  time2 = request.GET.get("search_time2")+':00.000000'
                  date1 = date[0].split('/')
                  date2 = date[1].split('/')
                  f_date1 = date1[2]+'-'+date1[0]+'-'+date1[1]+' '+time1
                  f_date2 = date2[2]+'-'+date2[0]+'-'+date2[1]+' '+time2
                  print(f_date1)
                  print(f_date2)
            contract = request.GET.get("contract")
            docdate = request.GET.get("docdate")
            f_docdate1 = None
            f_docdate2 = None
            terminal = request.GET.get("terminal")
            idtran = request.GET.get("idtran")
            orderid = request.GET.get("orderid")
            rrn = request.GET.get("rrn")
            sec = request.GET.get("sec")
            verstatus = request.GET.get("verstatus")
# print('0000000000000000')
            print(docdate)
            if docdate != '':
                docdatesplit = docdate.split('/')
                f_docdate1 = docdatesplit[2]+'-'+docdatesplit[0]+'-'+docdatesplit[1]
                f_docdate2 = docdatesplit[2]+'-'+docdatesplit[0]+'-'+docdatesplit[1]
            print(f_docdate1)
            print(f_docdate2)
            searchfilter = {}
            if f_date1 != None and f_date2 != None:
                searchfilter['TxnDateTime__gte']=f_date1
                searchfilter['TxnDateTime__lte']=f_date2
            if f_docdate1 != None and f_docdate2 != None:
                searchfilter['DocDate__gte']=f_docdate1
                searchfilter['DocDate__lte']=f_docdate2
            if contract != '':
                searchfilter['Contract']=contract
            if terminal != '':
                searchfilter['Terminal']=terminal
            if idtran != '':
                searchfilter['ID']=idtran
            if orderid != '':
                searchfilter['OrderID']=orderid
            if rrn != '':
                searchfilter['RRN']=rrn
            if sec != '':
               searchfilter['Sequence']=sec
            if verstatus != '':
               searchfilter['VerifyStatus']=verstatus


####loadsearch
            if searchfilter != {}:
                print('Non empty filter!')
                trans = RegistryModel.objects.filter(**searchfilter) #.order_by('-TxnDateTime')
#                print(trans)
                current_page = Paginator(trans,15)

                try:
                    context['trans'] = current_page.page(page)  
                except PageNotAnInteger:
                    context['trans'] = current_page.page(1)  
                except EmptyPage:
                    context['trans'] = current_page.page(current_page.num_pages) 


        return render(request, template_name=self.template_name, context=context)
    
    def post(self, request, *args, **kwargs):
        context['trans'] = trans 

        return render(request, template_name=self.template_name, context=context)

