from django.db import models
from django.contrib.auth.models import User
import uuid
import datetime
import django.utils
# Create your models here.


class EventsModel(models.Model):
    class Meta:
        db_table = "events" 
        permissions = (
            ('eventR', "Read events"),
            ('eventW', "Write events"),
            ('eventD', "Delete events"),
            ('eventC', "Change events"),
        )    
    
    TYPES = (
        "NotInRegistries",
        "NotInProcessing",
        "TypesDoNotMatch",
    )

    UNSOLVED = 0
    SOLVED = 1
    DEFERRED = 2

    STATUSES = (
        (UNSOLVED, 'Unsolved'),
        (SOLVED, 'Solved'),
        (DEFERRED, 'Deferred'),
    )

#    Id = models.BigIntegerField(unique = True, db_index=True, db_column='id', db_tablespace='pg_default') 
    CreatedAt = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='created_at', db_tablespace='pg_default') 
    UpdatedAt = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='updated_at', db_tablespace='pg_default') 
    DeletedAt = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='deleted_at', db_tablespace='pg_default') 
    TxnType = models.TextField(unique = True, db_index=True, db_column='txn_type', db_tablespace='pg_default') 
    Amount = models.BigIntegerField(unique = True, db_index=True, db_column='amount', db_tablespace='pg_default')
    TxnDateTime = models.DateTimeField(unique = True, default = django.utils.timezone.now, db_index=True, db_column='txn_date_time', db_tablespace='pg_default')
    RRN = models.TextField(null = False, db_index=True, db_column='rrn', db_tablespace='pg_default')
    OrderID = models.TextField(null = False, db_index=True, db_column='order_id', db_tablespace='pg_default')
    Type = models.IntegerField(null = False, db_column='type')
    Status = models.IntegerField(null = False, choices = STATUSES, default = UNSOLVED, db_column='status')

    def __str__(self):
        return str(self.UID)
