from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
import datetime
# Create your views here.

##############CLEARING PAGE################
@method_decorator(login_required, name='dispatch')
class ClearingEventsView(View):
    template_name = 'clearingtable.html'

    def get(self, request, *args, **kwargs):
        context = {}
        trans = EventsModel.objects.all().order_by('-TxnDateTime')
        
        current_page = Paginator(trans,15)
        page = request.GET.get('page')
        try:
            context['trans'] = current_page.page(page)  
        except PageNotAnInteger:
            context['trans'] = current_page.page(1)  
        except EmptyPage:
            context['trans'] = current_page.page(current_page.num_pages) 
            
        return render(request, template_name=self.template_name, context=context)




@method_decorator(login_required, name='dispatch')
class Clearing2View(View):
    template_name = 'clearing2.html'

    def get(self, request, *args, **kwargs):

        return render(request, template_name=self.template_name)


