from django.conf.urls import url

from . import views

app_name = 'app_clearing'
urlpatterns = [
  url(r'^$', views.ClearingEventsView.as_view()),
  url(r'^clearing2', views.Clearing2View.as_view()),

]
