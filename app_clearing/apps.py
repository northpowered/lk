from django.apps import AppConfig


class AppClearingConfig(AppConfig):
    name = 'app_clearing'
